# États {.inverse}

----

Les états du système de fichiers

![](images/etats.png){fig-align="center"}


## États (session 1)

![](images/etats_session1.png){fig-align="center"}

- *Working directory* : dossier de travail
- *Stage* : index pour tracer les modifications qui seront validées
- *Repo* : ensemble des fichiers (les vôtres et ceux de Git)
- *HEAD* : pointeur identifiant le dernier commit

