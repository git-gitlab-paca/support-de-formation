# Support de formation

Le code source comprend le contenu d'une présentation sur Git.

Chaque session dispose d'une présentation écrite dans des dossiers séparés.

# Description technique

Les supports de formation sont créés à l'aide de [Quarto](https://quarto.org/) pour un rendu avec la bibliothèque JavaScript [RevealJS](https://revealjs.com/) dans un fichier unique par session.

Le diaporama est publié sur GitLab Pages, fonctionnalité intégrée à la forge, à <https://git-gitlab-paca.pages.mia.inra.fr/support-de-formation/>.

# Modification en local

Pour construire les diaporamas sur son poste :

- Installer Quarto : <https://quarto.org/docs/get-started/>
- `git clone git@forgemia.inra.fr:git-gitlab-paca/support-de-formation.git`
- `cd support-de-formation`
- Sous Linux : `quarto render` ou `make clean build` pour reconstruire toutes les sessions

`quarto preview <dossier>/index.qml` permet de
- construire la présentation
- en continu mettre à jour la présentation d'une session, définie par `<dossier>`
- afficher la présentation dans le navigateur.
Il suffit de rafraîchir le navigateur (touche F5) pour voir les modifications.

# Conventions

Respecter la typographie des noms des logiciels :

- Git
- Git-Gui
- GitLab
- RStudio
- VS Code

Utiliser le caractère espace insécable ` ` devant les signes de ponctuation double.

Utiliser le caractère `…` pour les points de suspension.

# Licence

Licence ouverte [Etalab 2.0](https://forgemia.inra.fr/git-gitlab-paca/support-de-formation/-/blob/main/LICENCE.pdf?ref_type=heads)

# FTLV

Le dossier FTLV contient des modèles d'annonce, avec, en particulier les objectifs et le programme de chaque session.

Rédigés par la FTLV du centre INRAE PACA (merci à toute l'équipe), vous pouvez adapter ces documents pour vos besoins locaux (centre, date, lieux, intervenants) afin d'assurer les annonces "officielles" pour vos sessions.
