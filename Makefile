SHELL := /usr/bin/env bash

BUILD_DIR := public
SESSIONS := 01_initiation 02_gitlab_depot_distant 03_branches
SESSION_DIRS := $(SESSIONS:%=$(BUILD_DIR)/%)
INDEXES := $(SESSIONS:%=$(BUILD_DIR)/%/index.html)

build: $(INDEXES)

$(BUILD_DIR)/%/index.html:: %/index.qmd %/*.qmd
	@echo -e "###\n# $@\n###"
	quarto render $< --output-dir $(BUILD_DIR)

clean:
	rm -fr $(SESSION_DIRS)

run: build
	(sleep 1 ; python3 -mwebbrowser http://0.0.0.0:8000/) &
	(cd public/ ; python3 -m http.server)

