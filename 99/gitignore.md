---slide---

# .gitignore

* Fichier particulier, suivi, permettant d’exclure certains fichiers (filtres) du suivi du dépôt.
* Contient des patterns (ex. : `*.vsf`) ou des noms de fichiers explicites (ex. : `toto.dat`).
* Ajouter un pattern dans ce fichier n’a pas d’impact sur le fichier lui même dans le repo local. Il est simplement ignoré de la gestion.

---vertical---

# Ignorer des fichiers

Il est préférable de ne pas ajouter au dépôts certains fichiers, par exemple :

- Les fichiers de log
- Les fichiers résultant d'une compilation
- Les fichiers librairies
- Les fichiers de configuration, notamment s'ils contiennent des identifiants et mots de passe

On spécifie les fichiers à ignorer dans le fichier `.gitignore`

---vertical---

Syntaxe du fichier `.gitignore`

```
# Ignorer un répertoire
lib/

# Ignorer les fichiers ayant un nom particulier 
toto.txt

# Préfixer avec un slash pour indiquer que le fichier est à la racine du dépôt
/toto.txt

# Préfixer par un point d'exclamation pour explicitement ne pas ignorer
# Ici ignorer tous les fichiers du répertoire documentation sauf
# ceux avec l'extension .md
/documentation/*
!/documentation/*.md
```

Le fichier `.gitignore` est enregistré à la racine du dépôt et on le **commite**.

---vertical---

## Bonne pratique

Pour ignorer les fichiers de gestion de projet de l'IDE, on préférera utiliser le fichier `.gitignore_global` dont les chemin est spécifié avec l'option de configuration `core.excludesfile`.
Ce fichier suit la même syntaxe que `.gitignore`.
