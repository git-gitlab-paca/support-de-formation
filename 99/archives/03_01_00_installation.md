---slide---

# Installation

* Windows : <https://git-scm.com/download/win>
* Linux : `sudo apt-get install git`
* Mac : <https://git-scm.com/download/mac>
* Interface client (optionnelle) :
	* [tortoisegit](https://tortoisegit.org/)
