---slide---

# Voir l'historique du dépôt (Rstudio)

---vertical---

Exercice: prendre le temps de faire quelques commits supplémentaires en ajoutant/modifiant des fichiers sur le dépôt

---vertical---

Dans l'onglet git de RStudio, il y a un bouton "History" :

![Bouton history de l'interface RStudio](images/03_02_04_bouton_history.png)

---vertical---

Ce bouton permet d'accéder à l'historique du dépôt (tous les commits effectués dessus) :

![Historique du dépôt dans l'interface dédiée](images/03_02_04_history.png)

---vertical---

En sélectionnant un commit, on peut voir les informations qui le concernent :

![Vue d'un commit dans l'interface dédiée](images/03_02_04_vue_commit.png)

---vertical---

Il est aussi possible de voir un fichier tel qu'il était suite à un commit particulier :

![Bouton pour voir un fichier d'après un commit](images/03_02_04_vue_fichier_commit_bouton.png)

---vertical---

On peut alors parcourir un "instantané" du fichier :

![Vue d'un fichier d'après un commit](images/03_02_04_vue_fichier_commit.png)

---vertical---

Si l'on souhaite récupérer cet instantané, on peut l'enregistrer comme fichier :

![Enregistrer un fichier correspondant à un fichier particulier](images/03_02_04_enregistrer_fichier_commit.png)

---vertical---

Pour voir l'historique des commits sur un dépôt, on peut utiliser les commandes suivantes :

```bash
# Affiche l'historique des commits, avec un hash pour chacun
git log
# Affiche les modifications du commit correspondant au hash
git show <hash du commit intéressant>
```
