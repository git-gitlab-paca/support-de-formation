---slide---

# Ajouter des fichiers et leurs modifications sur le dépôt (Rstudio)

Créer des commits

---vertical---

Un commit permet :

- D'ajouter sur le dépôt des fichiers qui ne sont pas encore suivis
- D'enregistrer des modifications réalisées sur des fichiers suivis

---vertical---

Pour créer un commit, cliquer sur le bouton "Commit" :

![Bouton de commit de l'interface RStudio](images/03_02_03_bouton_commit.png)

Il existe aussi le raccourci clavier : `Ctrl+Alt+M`

---vertical---

Une fenêtre s'ouvre permettant de voir les fichiers non suivis ou les fichiers avec des modifications :

![Interface de commit](images/03_02_03_interface_commit.png)

---vertical---

Pour créer un commit contenant le fichier `.gitignore` :
1. Sélectionner le fichier `.gitignore`
1. Écrire un message de commit
1. Cliquer sur le bouton "Commit"

![Ajouter le gitignore au dépôt](images/03_02_03_ajouter_gitignore.png)

---vertical---

Si une erreur survient avec le message suivant :

```text
Please tell me who you are

…
```

C'est que votre identité n'est pas connue de git (les commit sont signés par des auteurs).
Aller dans le terminal de la fenêtre RStudio et utilisez les deux commandes suivantes pour configurer git :

```bash
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```

---vertical---

![Configuration du nom et du mail dans git](images/03_02_03_configuration_git.png)

On peut ensuite re-tenter de créer le commit dans la fenêtre de commit

---vertical---

Si tout se passe bien, on obtient un message ressemblant à celui-ci :

![Vue de l'interface suite à un commit réussi](images/03_02_03_commit_succes.png)

On peut alors fermer la fenêtre de commit

---vertical---

Nous allons modifier le fichier `.gitignore` pour qu'il ignore les fichiers ayant l'extension `.Rproj`.

Pour cela, ouvrir le fichier `.gitignore` et ajouter la ligne suivante :


```text
*.Rproj
```

![Modification du fichier gitignore](images/03_02_03_modification_gitignore.png)

---vertical---

On constate que l'onglet dépôt a été mis à jour :
- Le fichier `.Rproj` du projet a disparu
- Le fichier `.gitignore` a été modifié (petite icône bleue avec un "M" à l'intérieur)

![Nouvel état du dépôt](images/03_02_03_nouvel_etat_depot.png)

Nous allons faire un nouveau commit pour ajouter ces modifications au dépôt

---vertical---

Cliquer sur le bouton "Commit" et faire un commit pour cette modification

![Nouveau commit pour les modifications du gitignore](images/03_02_03_second_commit.png)

---vertical---

Bonne pratique pour les commits :
- À intervalle régulier
- Approche "atomique"
- Un changement dans le code = un commit
- Mauvaise pratique : un commit en fin de journée

---vertical---

Messages de commit :
- Commentaires courts, informatifs et explicites sur le changement présents dans le commit
- Mauvaise pratique : `Update code.r`
- Mauvaise pratique : `Petit rajout`
- Mauvaise pratique : `Code réparé`

---vertical---

Commande correspondante pour ajouter des modifications sur le dépôt :

```{bash}
# Modifier le fichier mon_code.r
$ git status # On constate que le fichier mon_code.r est identifié comme modifié
$ git add mon_code.r
$ git commit -m "Mon message de commit"
```

---vertical---

Git permet aussi aussi de suivre les suppressions et rennomages de fichiers :

```
# Déplacer un fichier
$ git mv fichier.txt new_name.txt
$ git commit -m "Mon message de commit"
# Supprimer un fichier
$ git rm fichier.txt new_name.txt
$ git commit -m "Mon message de commit"
```
