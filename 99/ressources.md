---slide---

# Ressources pédagogiques

- https://rogerdudler.github.io/git-guide/index.fr.html
- Doc en ligne : https://git-scm.com/
  - [L'aide mémoire](https://training.github.com/downloads/fr/github-git-cheat-sheet/)
  - [L'aide mémoire visuel](https://training.github.com/downloads/fr/github-git-cheat-sheet/)
  - [Le Livre Git](https://git-scm.com/book/fr/v2)
- <https://docs.gitlab.com/>
- [Using Git source control in VS Code](https://code.visualstudio.com/docs/sourcecontrol/overview)
- *Forges de l’Enseignement supérieur et de la Recherche - Définition, usages, limitations rencontrées et analyse des besoins*, Daniel Le Berre, Jean-Yves Jeannas, Roberto Di Cosmo, François Pellegrini ; 2023 ; [hal-04098702](https://hal-lara.archives-ouvertes.fr/hal-04098702) ; DOI [10.52949/34](https://dx.doi.org/10.52949/34)
