## Avec VS Code {#sec-ligne_commande_vscode}

![](../VS_code_logo.png){.absolute top=-10 right=0 width="8%"}

## Interface

![](../VS_code_logo.png){.absolute top=-10 right=0 width="8%"}

![](images/05_04_vscode_IHM.png)


## Création

![](../VS_code_logo.png){.absolute top=-10 right=0 width="8%"}

![](images/05-04_vscode_nvlle_branche.png){width=80%}

- crée une branche nommée `patch-1`
- bascule dessus

::: {.notes}
`git branch patch-1 ; git checkout patch -1` ou `git checkout -b patch-1` avant git-2.23
:::

<!--
```mermaid
gitGraph
   commit
   commit
   branch patch-1
   checkout patch-1
   commit
```
-->

![](images/mermaid_6.png){width=40%}

::: {.notes}
Cela déplace HEAD pour le faire pointer vers la branche testing.
La branche `patch-1` a avancé tandis que la branche `main` pointe toujours sur le commit sur lequel vous étiez lorsque vous avez lancé la commande `git checkout` pour changer de branche.
Les branches ne coûtent quasiment rien à créer et à détruire.
:::

## À partir d'une étiquette existante (1/2)

![](../VS_code_logo.png){.absolute top=-10 right=0 width="8%"}

- Créer une étiquette `v2.9.1`

![](images/05-04_vscode_create_tag1.png)


## À partir d'une étiquette existante (2/2)

![](../VS_code_logo.png){.absolute top=-10 right=0 width="8%"}

- Clic sur la branche en cours
- Menu "Create branch from ..."
- Sélection de l'étiquette `v2.9.1`
- Nommage de la nouvelle branche `patch-1`
- Bascule dessus automatiquement


<!--
```mermaid
gitGraph
   commit
   commit
   commit tag: "v2.9.1"
   branch patch-1
   checkout patch-1
   commit
   checkout main
   commit
   commit
```
-->

![](images/mermaid_7.png)

## Lister les branches

![](../VS_code_logo.png){.absolute top=-10 right=0 width="8%"}

![](images/05-04_vscode_liste_branches.png)


## Supprimer une branche locale

![](../VS_code_logo.png){.absolute top=-10 right=0 width="8%"}

![](images/05-04_vscode_suppr_branche.png){width=50%}

- sélectionner la branche à supprimer
- la branche en cours ne peut être supprimée

## Travailler avec plusieurs branches simultanément

![](../VS_code_logo.png){.absolute top=-10 right=0 width="8%"}

Basculer d'une branche à une autre

![](images/05_04_vscode_IHM.png)


::: {.notes}
Changer de branche modifie les fichiers dans votre répertoire de travail : le dossier se retrouve dans le même état que lors du dernier commit sur cette branche.
Il est important de noter que lorsque vous changez de branche avec Git, les fichiers de votre répertoire de travail sont modifiés. Si vous basculez vers une branche plus ancienne, votre répertoire de travail sera remis dans l’état dans lequel il était lors du dernier commit sur cette branche. Si git n’est pas en mesure d’effectuer cette action proprement, il ne vous laissera pas changer de branche.
[source](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Les-branches-en-bref)
:::