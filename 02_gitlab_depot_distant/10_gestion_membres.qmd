# Gestion des membres d'un projet GitLab {.inverse}

## Membres d’un projet

Par défaut seul le créateur du dépôt peut réaliser des modifications (*commit*).

Si dans le cadre d'une collaboration vous souhaitez travailler avec d'autres personnes il suffit de les ajouter comme membres de votre dépôt.

## Statut d’un membre

Les personnes qui auront accès à votre dépôt peuvent avoir différents rôles selon leur degré d'investissement dans le projet :

- *guest* : invité, consultation uniquement
- *reporter* : suivi et contribution au projet sans coder
- *developer* : contribuer au code sans configurer le projet GitLab
- *maintainer* : contribuer au code et configurer le projet GitLab
- *owner* : pour le transfert de propriété du projet

::: callout-warning
### Attention
Donner le rôle d'*owner* à un membre lui laissera le droit de pouvoir supprimer le dépôt…
:::

----

Pour inviter d’autres utilisateurs à contribuer au projet, il faut les inviter :

![](images/07_00_depot_distant_membres.png){fig-align="center"}

::: {.notes}
[Comment créer un compte sur ForgeMIA (hors ESR) ?](https://forgemia.inra.fr/adminforgemia/doc-public/-/wikis/FAQ#comment-cr%C3%A9er-un-compte-sur-forgemia-hors-esr-)
:::

------

Pour configurer le statut du membre :

![](images/07_00_depot_distant_membres_3.png)


Lorsqu'un utilisateur n'est pas connu de la forge, il faut lui demander de faire une première connexion sur l'adresse <a href="https://forgemia.inra.fr" target="_blank">ForgeMIA</a>, avec son identifiant LDAP.

Il est alors connu de la forge et vous pouvez l'ajouter sur le projet.


------

## Pratique

::: callout-tip
### Inviter un membre sur un projet

Invitez au moins une personne présente aujourd'hui à la formation sur votre projet personnel.

Ajoutez là en tant que *reporter* (ça suffira pour aujourd'hui).

:::
